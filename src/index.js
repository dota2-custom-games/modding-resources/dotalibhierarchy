import cytoscape from 'cytoscape';
import cola from 'cytoscape-cola';
cytoscape.use( cola );

// ------------------------------------------------------------- //
// --------------------- Graph Construction -------------------- //
// ------------------------------------------------------------- //


// Performs all the necessary graph construction steps
function RenderGraph(frameDiv, dataLoadingFunction)
{

    var graph = ConstructGraphFrame(frameDiv);
    var cy = LoadGraphIntoDiv(graph);

    // Add nodes and edges
    // In our initial load, all edges come from parents (representing rules)
    // and sub-nodes (e.g. individual facts) are orphaned.
    // This will make the layout process easier.

    /*
    cy.add({data: {id: '1'} })
    cy.add({data: {id: '2'} })

    cy.add({
    data:
    {
        target: '1',
        source: '2'
    }})
    */

    dataLoadingFunction(cy);

    // Applies the DAGRE layout to top level facts
    LoadCustomLayout(cy);

    // Move edges and nodes so that they belong to the correct parents
    AssignChildrenToParents(cy);

    // Move children around inside their parents
    AssignChildPositions(cy);
    // Affects user interaction
    DegrabifyChildren(cy);

    // Listen for clicks
    cy.on('mouseover', 'node', function(evt) {
        var node = evt.target;
        if (node.scratch('_descriptionText') != null)
        {
            console.log("Adding hover");
            node.addClass("onHover");
        }
    })
    cy.on('mouseout', 'node', function(evt) {
        var node = evt.target;
        if (node.scratch('_descriptionText') != null)
        {
            node.removeClass("onHover");
        }
    })

    cy.on('click', 'node', function(evt){
        var node = evt.target;
        if (node.scratch('_descriptionText') != null)
        {
            document.getElementById("descriptionBox").innerHTML = `
            <h2>`+node.scratch().label+"</h2>"+
            (node.scratch().credit ? "<h3>By "+node.scratch().credit+"</h3>" : "") + 
            (node.scratch().link ? "<h3><a href="+node.scratch().link+" target='_blank'>"+node.scratch().link+"</a></h3>" : "") + 
            node.scratch('_descriptionText');
        }
    })

}


// Organises the graph
// Proceeds in three steps, moving from top level nodes (i.e. rules) to bottom level (i.e. facts)
// This relies on us having correctly set the size of the top/midlevel nodes
function LoadCustomLayout(graph)
{
    //var layoutParams = DAGRE_LAYOUT_PARAMETERS;
    var layoutParams = COLA_LAYOUT_PARAMETERS;
    //var layoutParams = KLAY_LAYOUT_PARAMETERS;
    //var layoutParams = {name: 'klay'};
    console.log(layoutParams);
    graph.$(".topLevel").layout(layoutParams).run();
}

// Convert our graph json object into a cy object
// "Clean Code" told me that functions with only one line are
// in fact an example of excellent programming, not a confusing mess.
function LoadGraphIntoDiv(graph)
{
    var cy = cytoscape( graph );

    return cy
}


// Builds the json object which represents the parameters for the graph,
// such as style (And where to put it)
function ConstructGraphFrame(target)
{
    var graph = GRAPH_FRAME;

    graph['container'] = document.getElementById(target);

    return graph

}

// ------------------------------------------------------------- //
// ----------------------- Helper Functions -------------------- //
// ------------------------------------------------------------- //

// Simple node
function CreateNode(text){
    var node = {
        data: {
            //id: identifier,
            label: text
        }

    }

    return node
}

// Creates a node that is intended to contain a 'full' rule
// Fact collections are lists of objects {'id': string, 'text':string}
function CreateRuleNode(identifier, data){

    var adds = data["adds"];
    var states = data["states"];
    var modifies = data["modifies"];
    var extraClasses = data["extraClasses"];

    var newNodes = [];
    var parentNode = { 
            data: {id: identifier},
            classes: 'rule topLevel'
    };

    if (extraClasses) {
        parentNode.classes += " "+extraClasses;
    }

    // For setting width of the parent
    // This width will later be defined by the children positions,
    // but we will (over)approximate it here.
    // This is done so that the layout function will not cause weird overlaps between rules
    var maxWidth = "Modifies".length;

    var titleParent = {
        data: {
            id: identifier+"_title",
            label:identifier
        },
        classes: 'title',
        scratch: { _intendedParent: identifier, 
        '_descriptionText': data["descriptionText"],
        'label': identifier,
        'credit': data["credit"],
        'link': data["link"]
        }
    }
    //console.log("Title has description text: "+data["descriptionText"])

    newNodes.push(titleParent);

    if (adds.length > 0) {
    //if (true) {

        var addParent = { 
            data: { id: identifier+'_adds' },
            scratch: { _intendedParent: identifier },
            classes: 'factCollection' 
         };

        newNodes.push(addParent);


        var addTitle = { 
            data: { id: identifier+'_addstitle', label:"Adds" },
            scratch: { _intendedParent: identifier+'_adds' },
            classes: 'sectionTitle'
         };
         newNodes.push(addTitle);


        for (var add of adds)
        {
            var childNode = CreateNode(add['text']);

            var text;
            if (add['requiresInclude']) {
                text = "[¤] "+add['text'];
            } else {
                text = add['text'];
            }
            var childNode = {
                data: {
                    label: text
                },
                classes: 'fact',
                scratch: {
                    'label': add['text'],
                    '_descriptionText': add['descriptionText'],
                    '_intendedParent': identifier+'_adds'
                }
            }


            if( add['text'].length>maxWidth ){ maxWidth = add['text'].length };

            newNodes.push(childNode);
        }

    };

   if (modifies.length > 0) {

        var modParent = { 
            data: { id: identifier+'_Modifies' },
            scratch: { _intendedParent: identifier },
            classes: 'factCollection'
         };

        newNodes.push(modParent);


        var modTitle = { 
            data: { id: identifier+'_Modifiestitle', label:"Modifies" },
            scratch: { _intendedParent: identifier+'_Modifies' },
            classes: 'sectionTitle'
         };
         newNodes.push(modTitle);

        for (var mod of modifies)
        {
            var text;
            if (mod['requiresInclude']) {
                text = "[*] "+mod['text'];
            } else {
                text = mod['text'];
            }
            var childNode = {
                data: {
                    label: text
                },
                classes: 'fact',
                scratch: {
                    'label': mod['text'],
                    '_descriptionText': mod['descriptionText'],
                    '_intendedParent': identifier+'_Modifies'
                }
            }

            newNodes.push(childNode);

            if( mod['text'].length>maxWidth ){ maxWidth = mod['text'].length };
        }

    };

    //parentNode['style']['height'] = maxWidth*8;
    newNodes.push(parentNode);
    return newNodes

}

// Moves children around inside their parent
function AssignChildPositions(graph)
{
    graph.batch( function(){
        graph.$('.topLevel').forEach( function(topLevelNode){
            var previousParentWidth = topLevelNode.outerWidth();
            var widthChange = 0;
            var rollingSumY = 35;
            topLevelNode.children().forEach( function(midLevelNode) {
                midLevelNode.shift('x', (midLevelNode.outerWidth()-topLevelNode.width())/2)
                //console.log("Widthchange: "+widthChange+" for: "+midLevelNode.id())
                //console.log("Next y step");

                midLevelNode.shift('y', 30);
                //rollingSumY += midLevelNode.outerHeight();
                //console.log("Height shift: "+midLevelNode.outerHeight()+"for node: "+midLevelNode.id())
                midLevelNode.children().forEach( function (botLevelNode) {
                    //console.log("BotLeveLNode name: "+botLevelNode.id())
                    botLevelNode.shift('y', rollingSumY);
                    rollingSumY += botLevelNode.outerHeight();
                });

                midLevelNode.children().forEach( function(botLevelNode) {
                    //console.log("Shifting bot level node :"+botLevelNode.id()+" by: "+(botLevelNode.outerWidth()-midLevelNode.width())/2)
                    botLevelNode.shift('x', (botLevelNode.outerWidth()-midLevelNode.width())/2)
                })


            });
        });
    });

}

// Given a cy object, iterate through all nodes, and if that node
// has scratch _intendedParent, change its parent
function AssignChildrenToParents(graph)
{
    graph.$('node').forEach( function(ele) {
        if (ele.scratch('_intendedParent') != null) {
            ele.position( 'x', graph.$("#"+ele.scratch('_intendedParent')).position('x')  )
            ele.position( 'y', graph.$("#"+ele.scratch('_intendedParent')).position('y')  )
            ele.move({ parent: ele.scratch('_intendedParent') })
        }
    });
}

// From stackexchange (https://stackoverflow.com/questions/32130023/how-to-lock-the-position-of-a-cytoscape-js-node-within-its-parent-node)
// Prevent any non-parent level nodes from being draggable
// Sadly, this does not have the ideal functionality (that dragging a non-parent "transfers" the click to the parent)
// I believe this is a cyto-js limitation, there are some git issues regarding it.
function DegrabifyChildren(graph)
{
    graph.nodes().nonorphans()
        .on('grab', function(){this.ungrabify();})
        .on('free', function(){this.grabify();})
}


// Parse the json data into a graph
// We do this in two sweeps, so that we don't accidentally try to assign edges to nodes that haven't been created yet
function ParseData(graph, data)
{
    // Sweep 1: Add nodes for each node
    for (var node of data)
    {
        graph.add(CreateRuleNode(node.id, node.data))
    }

    // Sweep 2: Add edges for each dependency
    for (var node of data)
    {
        if (node.data.dependencies) {
            for (var dep of node.data.dependencies) {
                console.log("Attempting to add an edge from: "+node.id+" to "+dep);
                graph.add({
                data:
                {
                    target: node.id,
                    source: dep
                }})
            }
        }
    }

}


// Example dataset 1
function LoadDotaLibraryData(graph)
{
    ParseData(graph, dotalibrarydata)
}


// ------------------------------------------------------------- //
// ---------------------------- IIVE --------------------------- //
// ------------------------------------------------------------- //


(function() {
    console.log("Initializing!");
    document.addEventListener('DOMContentLoaded', function () {

        RenderGraph("cyto-frame", LoadDotaLibraryData);

        console.log("Loaded!");
    });

})()
// ------------------------------------------------------------- //
// -------------------------- Constants ------------------------ //
// ------------------------------------------------------------- //

DAGRE_LAYOUT_PARAMETERS =
{
  name: 'dagre',
      // dagre algo options, uses default value on undefined
  nodeSep: 100, // the separation between adjacent nodes in the same rank
  edgeSep: 10, // the separation between adjacent edges in the same rank
  rankSep: 200, // the separation between adjacent nodes in the same rank
  rankDir: 'TB', // 'TB' for top to bottom flow, 'LR' for left to right,
  ranker: 'network-simplex', // Type of algorithm to assign a rank to each node in the input graph. Possible values: 'network-simplex', 'tight-tree' or 'longest-path'
  minLen: function( edge ){ return 1; }, // number of ranks to keep between the source and target of the edge
  edgeWeight: function( edge ){ return 1; }, // higher weight edges are generally made shorter and straighter than lower weight edges

  // general layout options
  fit: true, // whether to fit to viewport
  padding: 30, // fit padding
  spacingFactor: 1, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
  nodeDimensionsIncludeLabels: true, // whether labels should be included in determining the space used by a node
  animate: false, // whether to transition the node positions
  animateFilter: function( node, i ){ return true; }, // whether to animate specific nodes when animation is on; non-animated nodes immediately go to their final positions
  animationDuration: 500, // duration of animation in ms if enabled
  animationEasing: undefined, // easing of animation if enabled
  boundingBox: {x1:0, y1:0, x2:2000, y2:1000}, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
  transform: function( node, pos ){
    var x = pos['x'];
    var y = pos['y'];
    console.log("Node: "+node.id()+" has pos: "+"["+x+", "+y+"], in degree: "+node.indegree(true)+", out degree: "+node.outdegree(true));
    return {'x': 1.5*pos['x'], 'y':5*pos['y'] } 
}, // a function that applies a transform to the final node position
  // -- For some reason the graph doesn't layout properly unless we apply this bizarre transform.
  ready: function(){}, // on layoutready
  stop: function(){}
}

// default layout options
COLA_LAYOUT_PARAMETERS = {
  name: 'cola',
  animate: true, // whether to show the layout as it's running
  refresh: 1, // number of ticks per frame; higher is faster but more jerky
  maxSimulationTime: 4000, // max length in ms to run the layout
  //ungrabifyWhileSimulating: true, // so you can't drag nodes during layout
  fit: true, // on every layout reposition of nodes, fit the viewport
  padding: 40, // padding around the simulation
  //boundingBox: {x1:0, y1:0, x2: 1500, y2: 1000}, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
  nodeDimensionsIncludeLabels: true, // whether labels should be included in determining the space used by a node

  // layout event callbacks
  ready: function(){}, // on layoutready
  stop: function(){}, // on layoutstop

  // positioning options
  randomize: true, // use random node positions at beginning of layout
  avoidOverlap: true, // if true, prevents overlap of node bounding boxes
  //handleDisconnected: true, // if true, avoids disconnected components from overlapping
  //convergenceThreshold: 0.01, // when the alpha value (system energy) falls below this value, the layout stops
  flow: {axis: 'x', minSeparation: 200}, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
  nodeSpacing: function( node ){ return 150; }, // extra spacing around nodes
  //alignment: undefined,//function( node ){ return { x: 0, y: 1 } }, // relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }
  //gapInequalities: undefined, // list of inequality constraints for the gap between the nodes, e.g. [{"axis":"y", "left":node1, "right":node2, "gap":25}]

  // different methods of specifying edge length
  // each can be a constant numerical value or a function like `function( edge ){ return 2; }`
  edgeLength: undefined, // sets edge length directly in simulation
  edgeSymDiffLength: undefined, // symmetric diff edge length in simulation
  edgeJaccardLength: undefined, // jaccard edge length in simulation

  // iterations of cola algorithm; uses default values on undefined
  unconstrIter: 10, // unconstrained initial layout iterations
  userConstIter: 20, // initial layout iterations with user-specified constraints
  allConstIter: undefined, // initial layout iterations with all constraints including non-overlap

  // infinite layout options
  infinite: false // overrides all other options for a forces-all-the-time mode
};

// This contains the necessary CSS styles for making the graph pretty
GRAPH_FRAME = {
    elements: [],
    style: `
        node {
        }

        node.rule {
            padding: 2px;
            background-color: #D0D1EF;
            overlay-padding: 4px;
            border-width: 1px;
            border-color: black;
        }
        node.rule.incomplete {
            background-color: #EFD0D1;
            width: 80px;
            height: 40px;
            padding: 12px;
            padding-bottom: 32px;
        }
        node.title{
            width: label;
            height: 1px;
            padding: 10px;
            shape: square;
            label: data(label);
            text-valign: center;
            text-halign: center;
            overlay-padding: 0px;
            overlay-opacity: 0;
            background-opacity: 0;
            color: #0D1B2A;
            font-size: 22px;
            font-weight: bold;
        }
        node.title.onHover{
            color: #2B86CB;
        }
        node.sectionTitle{
            width: label;
            height: 1px;
            font-size: 18px;
            padding: 10px;
            shape: square;
            label: data(label);
            text-valign: center;
            text-halign: center;
            overlay-padding: 4px;
            overlay-opacity: 0;
            background-opacity: 0;
            color: #1B263B;
            font-weight: bold;
        }
        node.fact{
            width: label;
            height: 10px;
            padding: 10px;
            shape: square;
            label: data(label);
            color: #617A97;
            text-valign: center;
            text-halign: center;
            overlay-padding: 0px;
            overlay-opacity: 0;
            background-opacity: 0;
        }
        node.fact.onHover{
            color: #1B263B;
        }
        node.factCollection{
            width: label;
            height: 10px;
            text-valign: top;
            text-halign: center;
            background-color: #B7CDE9;
            border-width: 1px;
            border-color: black;
            padding: 0px;
            overlay-padding: 0px;
            overlay-opacity: 0;
            background-opacity: 1;
        }
        edge {
            width: 2px;
            line-color: black;
            overlay-opacity: 0;
            target-arrow-shape: triangle;
            arrow-scale: 1;
            target-arrow-color: black;
            curve-style: bezier;
        }
        core {
            active-bg-size: 0;
        }
    `,
    userZoomingEnabled: true,
    panningEnabled: true,
    boxSelectionEnabled: false,
    wheelSensitivity: 0.25
}

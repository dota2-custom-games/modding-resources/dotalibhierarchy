var dotalibrarydata = [
// ----------------------------------- //
// ---------- Mark: Timers ----------- //
// ----------------------------------- //
    {
        id: "Timers",
        data: {
            dependencies: [],
            credit: "BMD",
            link: "",
            descriptionText: `
                    The timers library gives the ability to create timers that (repeatedly) perform
                    a function after a given delay. DotA natively allows such
                    functionality with the use of thinkers, but these can often lead to
                    memory leaks or bugs if misused. The library provides a convenient wrapper
                    that helps mitigate the risk of such problems.
            `,
            adds: [
                {text:"libraries/timers.lua",
                requiresInclude: true,
                descriptionText: `
                    Contains all functionality.
                `}
            ],
            modifies: []
        }
    },

// ----------------------------------- //
// ------ Mark: PlayerTables --------- //
// ----------------------------------- //
    {
        id: "PlayerTables",
        data: {
            dependencies: [],
            credit: "BMD",
            link: "",
            descriptionText: `
                Grants nettable-like functionality that is shared only with specific players,
                instead of all of them.
            `,
            adds: [
                {text:"libraries/playertables.lua",
                requiresInclude: true,
                descriptionText: `
                    Main luaside functionality
                `},
                {text:"panorama/scripts/playertables_base.js",
                requiresInclude: true,
                descriptionText: `
                    Performs panoramaside work
                `}
            ],
            modifies: []
        }
    },

// ----------------------------------- //
// -------- Mark: Containers --------- //
// ----------------------------------- //
    {
        id: "Containers",
        data: {
            dependencies: ["PlayerTables", "Timers"],
            credit: "BMD",
            link: "",
            descriptionText: `
                Allows for use of item slots in panorama elements, such as inventories or shops.
            `,
            adds: [
                {text:"libraries/containers.lua",
                requiresInclude: true,
                descriptionText: `
                    Main luaside functionality
                `},
                {text:"panorama/layout/container_base.xml",
                requiresInclude: true,
                descriptionText: `
                    Main hook for panorama code
                `},
                {text:"panorama/scripts/container_base.js",
                descriptionText: `
                    Global manager for all functionality
                `},
                {text:"panorama/scripts/container.js",
                descriptionText: `
                    Main script functions for an individual container
                `},
                {text:"panorama/styles/container.css"}
            ],
            modifies: []
        }
    },


// ----------------------------------- //
// ----- Mark: CustomInventory ------- //
// ----------------------------------- //
    {
        id: "CustomInventory",
        data: {
            dependencies: ["Containers"],
            credit: "",
            link: "",
            extraClasses: "incomplete",
            descriptionText: `
                Replaces the core dota inventory with a custom one, allowing further extension.
            `,
            adds: [],
            modifies: []
        }
    },


// ----------------------------------- //
// ----- Mark: CustomShops ------- //
// ----------------------------------- //
    {
        id: "CustomShops",
        data: {
            dependencies: ["Containers"],
            credit: "",
            link: "",
            extraClasses: "incomplete",
            descriptionText: `
                Allows for customisable shops attached to units.
            `,
            adds: [],
            modifies: []
        }
    },


// ----------------------------------- //
// ------- Mark: BottomPanel --------- //
// ----------------------------------- //
    {
        id: "BottomPanel",
        data: {
            dependencies: ["CustomStats"],
            credit: "",
            link: "",
            extraClasses: "incomplete",
            descriptionText: `
                Gives a custom replacement to the DotA bottom panel
            `,
            adds: [],
            modifies: []
        }
    },


// ----------------------------------- //
// -------- Mark: CustomStats -------- //
// ----------------------------------- //
    {
        id: "CustomStats",
        data: {
            dependencies: [],
            credit: "",
            link: "",
            descriptionText: `
                Fully replaces the DotA stat system 
            `,
            adds: [
                {text:"libraries/CustomStats.lua",
                requiresInclude: true,
                descriptionText: `
                    Contains all functionality.
                `}
            ],
            modifies: [
                {text:"custom_net_tables.txt",
                requiresInclude: true,
                descriptionText: `
                    Add nettable 'custom_stats'.
                `}]
        }
    },


// ----------------------------------- //
// ------- Mark: ProgressBars -------- //
// ----------------------------------- //
    {
        id: "ProgressBars",
        data: {
            dependencies: ["Timers"],
            credit: "",
            link: "",
            descriptionText: `
                Allows for progress bars that appear above units, tracking with modifiers.
            `,
            adds: [
                {text:"libraries/ProgressBars.lua",
                requiresInclude: true,
                descriptionText: `
                    Contains luaside functionality.
                `},
                {text:"panorama/layout/progressbars.xml",
                requiresInclude: true,
                descriptionText: `
                    Contains panoramaside hooks.
                `},
                {text:"panorama/styles/progressbars.css",
                requiresInclude: false
                },
                {text:"panorama/scripts/progressbars.js",
                requiresInclude: false
                }
            ],
            modifies: [
                {text:"custom_net_tables.txt",
                requiresInclude: true,
                descriptionText: `
                    Add nettable 'progress_bars'.
                `}]
        }
    },


// ----------------------------------- //
// -------- Mark: Selection ---------- //
// ----------------------------------- //
    {
        id: "Selection",
        data: {
            dependencies: ["Timers"],
            credit: "Noya",
            link: "",
            descriptionText: `
                    Grants some utility functions for setting or changing a player's selected
                    units. Note that partial functionality can be attained from just the lua file - 
                    the panorama side is only required for full functionality.
            `,
            adds: [
                {text:"libraries/selection.lua",
                requiresInclude: true,
                descriptionText: `
                    Contains all functionality.
                `},
                {text:"panorama/layout/selection.xml",
                requiresInclude: true,
                descriptionText: `
                    Main panorama loader
                `},
                {text:"panorama/scripts/selection.js",
                descriptionText: `
                    Does actual panoramaside work
                `},
                {text:"panorama/scripts/selection_filter.js",
                descriptionText: `
                    Contains some utility functions
                `}
            ],
            modifies: [
                {text: "custom_net_tables.txt",
                descriptionText: `
                    Add table for 'selection'
                `
                }
            ]
        }
    },
// ----------------------------------- //
// ------ Mark: BuildingHelper ------- //
// ----------------------------------- //
    {
        id: "BuildingHelper", 
        data: {
            credit: "Noya",
            link: "",
            descriptionText: `
                    BuildingHelper allows for the creation of abilities that let units construct
                    towers and other buildings. 
            `,
            adds: [
                {text:"libraries/buildinghelper.lua",
                requiresInclude: true,
                descriptionText: `
                    Performs the body of work and interacts with the panoramaside.
                `},                
                {text:"builder.lua",
                descriptionText: `
                    Universal code for all 'build' abilities.
                `},
                {text:"repair.lua",
                descriptionText: `
                    Universal code for all 'repair' abilities.
                `},
                {text:"modifier_out_of_world.lua",
                descriptionText: ``
                },
                {text:"panorama/layout/buildinghelper.xml",
                requiresInclude: true,
                descriptionText: `
                    Root of panorama functions
                `},  
                {text:"panorama/scripts/clicks.js",
                descriptionText: `
                    Listener for click events for placing build orders
                `},
                {text:"panorama/scripts/buildinghelper.js",
                descriptionText: `
                    Does the actual work of panoramaside
                `},   
                {text:"materials/grid_overlay.vtex",
                requiresInclude: true,
                descriptionText: `
                    Shows the building grid. Can also just copy compiled version to avoid include.
                `},
                {text:"materials/range_overlay.vtex",
                requiresInclude: true,
                descriptionText: `
                    Shows range indicator of buildings. Can also just copy compiled version to avoid include.
                `},
                {text:"particles/ghost_model.vtex",
                requiresInclude: true,
                descriptionText: `
                    Ghost of buildings before they are placed.
                `},
                {text:"particles/square_overlay.vtex",
                requiresInclude: true},                                           
                {text:"particles/range_overlay.vtex",
                requiresInclude: true},                                           
                {text:"particles/square_sprite.vtex",
                requiresInclude: true},                                           
                {text:"particles/square_projected.vtex",
                requiresInclude: true},                                                                                                                               
            ],
            modifies: [
                {text: "custom_net_tables.txt",
                descriptionText: `
                    Add tables for 'builders', 'construction_size', 'building_settings'
                `
                }
            ],
            dependencies: ["Notifications", "Selection"]
        }
    },
// ----------------------------------- //
// ------ Mark: Notifications -------- //
// ----------------------------------- //
    {
        id: "Notifications",
        data: {
            credit: "BMD",
            link: "",
            descriptionText: `
            The notifications library contains a collection of simple functions to 
            display a range of notifications to players. 
            `,
            adds: [
                {text:"libraries/notifications.lua",
                requiresInclude: true,
                descriptionText: `
                    Contains luaside functions.
                `},
                {text:"panorama/layout/notifications.lua",
                requiresInclude: true,
                descriptionText: `
                    Main panoramaside hook.
                `},
                {text:"panorama/styles/notifications.css",
                requiresInclude: false},
                {text:"panorama/scripts/util_sprintf.js",
                requiresInclude: false,
                descriptionText: `
                    Adds utility function for formatting text
                `},
                {text:"panorama/scripts/notifications.js",
                requiresInclude: false,
                descriptionText: `
                    Receives calls from the luaside
                `}
            ],
            modifies: [],
            dependencies: []
        }
    }
];